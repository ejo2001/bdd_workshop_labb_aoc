Feature: Testing

	Scenario Outline: Entering input

			Given I enter <inputs>

			Then I want <results>

			Examples:
			|inputs|results|
			|1122|3|
			|1111|4|
			|1234|0|
			|91212129|9|
