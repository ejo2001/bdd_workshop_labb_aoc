Feature: Testing

	Scenario Outline: Entering input

			Given I enter <inputs>

			Then I want <results>

			Examples:
			|inputs|results|
			|1212|6|
			|1221|0|
			|123426|4|
			|123123|12|  
			|12131415|4|
