from pytest_bdd import scenarios, given, when, then, parsers
import main_day1_part2 as main


scenarios("user_story_day1_part2.feature")

@given(parsers.parse("I enter {inputs:d}"), target_fixture="output")
def run_function(inputs):
	return main.solve_captcha(str(inputs))

@then(parsers.parse('I want {results:d}'))
def return_output(output, results):
	assert output == results
