FROM python:3.10

WORKDIR /app

COPY ./* /app/

RUN pip install -r /app/requirements.txt

ENTRYPOINT ["python3", "/app/*.py"]

