def return_highest(inp):
    highest = 0
    lowest = 10000
    numbs = inp.split()
    for character in numbs:
        if int(highest) <= int(character):
            highest = int(character)
        
        if int(lowest) >= int(character):
            lowest = int(character)
   

    print("Original: " + str(inp))
    print("Highest: " + str(highest))
    print("Lowest: " + str(lowest))
    tot = highest - lowest
    return int(tot)



def solve_checksum(inp):
    tot = 0
    for number in inp:
        tot += return_highest(number)

    return int(tot)
