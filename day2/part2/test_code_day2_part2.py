
from pytest_bdd import scenarios, given, when, then, parsers
import main_day2_part2 as main


scenarios("user_story_day2_part2.feature")

@given(parsers.parse("I enter {inputs}"), target_fixture="output")
def run_function(inputs):
    numbers = inputs.split()
    for count, inp in enumerate(numbers):
        c = ""
        for i in inp:
            c += i + " "
        numbers[count] = c

    print(numbers)
    return main.solve_checksum(numbers)

@then(parsers.parse('I want {results}'))
def return_output(output, results):
    assert int(output) == int(results)
