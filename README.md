# BDD

Compared to TDD, BDD got quite complicated. It became complicated as I now needed an extra file for input, and I needed a whole different structure off my tests. However, I quite enjoyed working in it once I got the hang of it, and the user stories made everything better in terms of documentation and giving directions of where I wanted to go with my projects.
